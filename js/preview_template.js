(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.contact_mail_formatter = {
      attach: function (context, settings) {

        $('#mail-formater textarea').on({
          keypress: function () {
            if (($("[name='template']").val()) != 'custom') {
              var mail_html = drupalSettings.mailHtml;
              $("[name='template'] ").val("custom");
              $('#edit-mail-html-value').attr('value', mail_html);
            }
          }
       });
      }
    };
  })(jQuery, Drupal, drupalSettings);

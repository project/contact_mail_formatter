# Contact Mail Formatter


## Contents of This File

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## Introduction

Contact Mail Formatter module will provide HTML Mail for Contact messages which
are fully-fledged entities in Drupal.

 - For a full description of the module, visit the project page:
   <https://www.drupal.org/project/contact_mail_formatter>

 - To submit bug reports and feature suggestions, or to track changes:
   <https://www.drupal.org/project/issues/contact_mail_formatter>


## Requirements

Core Contact Module, token & smtp module.

## Installation

 - Install the Contact Mail Formatter module as you would normally install
 a contributed
   Drupal module.
   Visit [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

## Configuration

    1. `Admin -> Extend` & enable the module.
    2. `Admin -> structure -> contact forms`.
    3. Navigate to the "Edit" tab of the contact form.
    4. In the "Mail Formatter" section, locate and check the flag for
    "Enable Mail Formatter."
    5. Utilize the provided sample HTML email templates.
    6. Optionally, override the template by entering custom HTML through
    the text area. Keypress in the text area to access the custom HTML template.
    7. Click on "Browse available tokens" to access and incorporate custom
    tokens in your HTML email template.
    8. For SMTP setup, go to Administer -> Configuration -> System -> SMTP
    Authentication Support page. Fill in the required settings to configure SMTP
    for seamless email functionality.

## Maintainers

 - Jitendra Verma - <https://www.drupal.org/u/jitendra-verma>

 - Ritvik tak - <https://www.drupal.org/u/ritviktak>

Supporting organizations:

 - TO THE NEW - <https://www.drupal.org/to-the-new>
